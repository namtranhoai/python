# 5. Given a list as follows: l = [23, 4.3, 4.2, 31, ‘python’, 1, 5.3, 9, 1.7]
# a. Remove the item “python” in the list.
# b. Sort this list by ascending and descending.
# c. Check either number 4.2 to be in l or not?

l = [23, 4.3, 4.2, 31, 'python', 1, 5.3, 9, 1.7]
# a.
l.remove('python')
print(l)

# b.
l.sort()
print(l)
l.reverse()
print(l)

#c.
print("Check either number 4.2 to be in l or not:")
print(l.count(4.2)>0)
