# 4. Write a python script to print the following
# string in a specific format. s = “Twinkle, twinkle, little star, How I wonder what you are! Up above the world so high, Like a diamond in the sky. Twinkle, twinkle, little star, How I wonder what you are”.  The output is:
# Twinkle, twinkle, little star,
# How I wonder what you are!
# Up above the world so high,
# Like a diamond in the sky.
# Twinkle, twinkle, little star,
# How I wonder what you are.

s = "Twinkle, twinkle, little star, \n\tHow I wonder what you are! \nUp above the world so high,\n\tLike a diamond in the sky. \nTwinkle, twinkle, little star,\n\tHow I wonder what you are."
print(s)
# The output is:
# Twinkle, twinkle, little star"