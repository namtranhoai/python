# Python Programming for Beginners
# Homework 2
#
# Write answer for the following questions in a python file using PyCharm and push it into your gitlab account.
# 3. Given a string s = “Hi John, welcome to python programming for beginner!”. Your mission is write a small python script to:
# a. Check a string “python” that either exists in s or not.
# b. Extract the word “John” from s and save it into a variable named s1.
# c. Count how many character ‘o’ in s and print it on console. Guide: use count() function of string.
# d. Count how many word in s and print it on console.
# Guide:  use split() function of string to split s to a list of strings and then use len() function to count the size of list.

s ="Hi John, welcome to python programming for beginner!"
# a.
print("python" in s)
# b.
s1=s[3:7:1]
print(s1)
# c.
print(s.count('o'))
# d.
words = s.split(" ")
print(len(words))
