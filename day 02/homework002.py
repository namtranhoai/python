# Python Programming for Beginners
# Homework 2
#
# Write answer for the following questions in a python file using PyCharm and push it into your gitlab account.
# 1. Given the following statement: u, v, x, y, z = 29, 12, 10, 4, 3. write result of the following expression:
u, v, x, y, z = 29, 12, 10, 4, 3
# a. u/v = ?
print(u/v)
# b. t = (u==v) => t = ?
t = (u == v)
print(t)
# c. u%x= ?
print(u%x)
# d. t= (x>=y) => t = ?
t = (x >= y)
print(t)
# e. u += 5 => u = ?
u += 5
print(u)
u -= 5
# f. u %= z => u = ?
u %= z
print(u)
u = 29
# g. t = (v>x and y<z) => t = ?
t = (v>x and y<z)
print(t)
# h. x**z = ?
print(x**z)
# i. x//z = ?
print(x//z)

