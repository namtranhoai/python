# Python Programming for Beginners
# Homework 2
#
# Write answer for the following questions in a python file using PyCharm and push it into your gitlab account.
# 2. Write a python script to compute the perimeter and the area of a circle with radius r = 5.
#

import math
def PrimeterCircle(r):
    return r*2*math.pi
def AreaCircle(r):
    return (r**2)*math.pi
r = 5
print(PrimeterCircle(r))

print(AreaCircle(r))